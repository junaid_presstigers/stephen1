function center_div(){    
    $('#sync1 .item').each(function(){        
        var slide_img = new Image();
        slide_img.src = $(this).find('.home-right-side img').attr('src');
        slide_img.onload = function(){
            $('.home-left-side').height($('.home-right-side').height());
            var content_top = ($('.home-left-side').height() / 2) - ($('.main-content').height() / 2);    
            $('.main-content').css('top',content_top + "px");
        }
    });
}

$(window).resize(function(){
    center_div();
});

$(document).ready(function(){ 
	var sync1 = $("#sync1");
	var sync2 = $("#sync2");
	
	sync1.owlCarousel({
		singleItem : true,
		navigation : false,
		pagination : false,
		margin     : 10,
                autoPlay   : true,
                autoplayHoverPause:true,
		responsiveRefreshRate : 200
	});
				
	sync2.owlCarousel({
		items : 3,
		itemsDesktop      : [1199,3],
		itemsDesktopSmall : [979,3],
		itemsTablet       : [768,2],
		itemsMobile       : [479,1],
		pagination        :false,
		responsiveRefreshRate : 100,
		afterInit : function(el){
		  el.find(".owl-item").eq(0).addClass("synced");
		}
	});
				
	function syncPosition(el){
		var current = this.currentItem;
		$("#sync2")
		  .find(".owl-item")
		  .removeClass("synced")
		  .eq(current)
		  .addClass("synced");
		if($("#sync2").data("owlCarousel") !== undefined){
		  center(current);
		}
	}
				
	$("#sync2").on("click", ".owl-item", function(e){
		e.preventDefault();
		var number = $(this).data("owlItem");
		sync1.trigger("owl.goTo",number);
	});
				
	function center(number){
		var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
		var num = number;
		var found = false;
		for(var i in sync2visible){
		  if(num === sync2visible[i]){
				var found = true;
		  }
		}
	
		if(found===false){
		  if(num>sync2visible[sync2visible.length-1]){
				sync2.trigger("owl.goTo", num - sync2visible.length+2);
		  }else{
				if(num - 1 === -1){
				  num = 0;
				}
				sync2.trigger("owl.goTo", num);
		  }
		} else if(num === sync2visible[sync2visible.length-1]){
		  sync2.trigger("owl.goTo", sync2visible[1]);
		} else if(num === sync2visible[0]){
		  sync2.trigger("owl.goTo", num-1);
		}
	
	}
        
        center_div();
        
});
